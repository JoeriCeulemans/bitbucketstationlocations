NOTE: DO THIS ON THE APPLICATION SERVER
01_Tpod_Tpod.Webapp
Copy folder Tpod.WebApp to F:\Tpod\ (if the folder Tpod.WebApp already exist, delete it first)

02_OIF_OIF config files
Copy <ENV>\oif folder to F:\TPOD\Tpod.WebAp

03_Tpod_Tpod.Sync
Copy folder Tpod.Sync to F:\Tpod\ (application server) (if the folder Tpod.Sync already exist, delete it first)

04 publish mxd tpodTarData in Address folder
publish ArcGIS mapservice 'tpodTarData ' using the standard procedure, publish in  Address folder with following capabilities : 
* mapping 
Note: If the server is upgraded to ArcGis 10.4, the mxd has to be published with ArcMap 10.4